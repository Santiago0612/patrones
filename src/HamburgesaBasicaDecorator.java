public class HamburgesaBasicaDecorator implements Hamburgesa{


    private final Hamburgesa hamburgesa;

    public HamburgesaBasicaDecorator(Hamburgesa hamburgesa) {
        this.hamburgesa = hamburgesa;
    }

    @Override
    public String armarHamburgesa() {
        this.hamburgesa.armarHamburgesa();
        System.out.println("Pan y Carne añadido");
        return "Pan y Carne añadido";
    }
}
