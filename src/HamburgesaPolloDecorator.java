public class HamburgesaPolloDecorator implements Hamburgesa{

    private final Hamburgesa hamburgesa;

    public HamburgesaPolloDecorator(Hamburgesa hamburgesa) {
        this.hamburgesa = hamburgesa;
    }

    @Override
    public String armarHamburgesa() {
        this.hamburgesa.armarHamburgesa();
        System.out.println("Pollo Añadido");
        return "Pollo Añadido";
    }
}
