public class HamburgesaConQuesoDecorator implements Hamburgesa{

    private final Hamburgesa hamburgesa;

    public HamburgesaConQuesoDecorator(Hamburgesa hamburgesa) {
        this.hamburgesa = hamburgesa;
    }

    @Override
    public String armarHamburgesa() {
        this.hamburgesa.armarHamburgesa();
        System.out.println("Queso añadido");
        return "Queso Añadido";
    }
}
