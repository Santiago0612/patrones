public class HamburgesaCompletaDecorator implements Hamburgesa{

    private final Hamburgesa hamburgesa;

    public HamburgesaCompletaDecorator(Hamburgesa hamburgesa) {
        this.hamburgesa = hamburgesa;
    }

    @Override
    public String armarHamburgesa() {
        this.hamburgesa.armarHamburgesa();
        System.out.println("Lechuga y tomate añadidos");
        return "Lechuga y tomate añadidos";
    }
}
