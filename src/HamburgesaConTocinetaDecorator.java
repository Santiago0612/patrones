public class HamburgesaConTocinetaDecorator implements Hamburgesa{
    private final Hamburgesa hamburgesa;

    public HamburgesaConTocinetaDecorator(Hamburgesa hamburgesa) {
        this.hamburgesa = hamburgesa;
    }


    @Override
    public String armarHamburgesa() {
        this.hamburgesa.armarHamburgesa();
        System.out.println("Tocineta y Arepa Añadidos");
        return "Tocineta y Arepa Añadidos";
    }
}
