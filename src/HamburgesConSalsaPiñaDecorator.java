public class HamburgesConSalsaPiñaDecorator implements Hamburgesa{

    private final Hamburgesa hamburgesa;

    public HamburgesConSalsaPiñaDecorator(Hamburgesa hamburgesa) {
        this.hamburgesa = hamburgesa;
    }

    @Override
    public String armarHamburgesa() {
        this.hamburgesa.armarHamburgesa();
        System.out.println("Piña Añadida");
        return "Piña Añadida";
    }
}
