public class Main {
    public static void main(String[] args) {
        Thread Hilo1 =new Thread(new Runnable() {
            @Override
            public void run() {
                HamburgesaBase hamburgesaBase = ControladorDeHamburgesas.getControlador();
                HamburgesaBasicaDecorator hamburgesaBasicaDecorator = ControladorDeHamburgesas.getControlador1();
                HamburgesaCompletaDecorator hamburgesaCompletaDecorator = ControladorDeHamburgesas.getControlador2();
                HamburgesaConQuesoDecorator hamburgesaConQuesoDecorator = ControladorDeHamburgesas.getControlador3();
                HamburgesaConSalsaBBQDecorator hamburgesaConSalsaBBQDecorator = ControladorDeHamburgesas.getControlador4();
                HamburgesaConTocinetaDecorator hamburgesaConTocinetaDecorator = ControladorDeHamburgesas.getControlador5();
                HamburgesaPolloDecorator hamburgesaPolloDecorator = ControladorDeHamburgesas.getControlador6();
                HamburgesConSalsaPiñaDecorator hamburgesConSalsaPiñaDecorator = ControladorDeHamburgesas.getControlador7();
                hamburgesConSalsaPiñaDecorator.armarHamburgesa();

                System.out.println(hamburgesaBase.hashCode());


            }

            //Aqui armamos otra configuracion o tipo de hamburgesas
        });

        Thread Hilo2 =new Thread(new Runnable() {
            @Override
            public void run() {
                HamburgesaBase hamburgesaBase = ControladorDeHamburgesas.getControlador();
                HamburgesaBasicaDecorator hamburgesaBasicaDecorator = ControladorDeHamburgesas.getControlador1();
                HamburgesaConQuesoDecorator hamburgesaConQuesoDecorator = ControladorDeHamburgesas.getControlador3();
                HamburgesaPolloDecorator hamburgesaPolloDecorator = ControladorDeHamburgesas.getControlador6();
                HamburgesConSalsaPiñaDecorator hamburgesConSalsaPiñaDecorator = ControladorDeHamburgesas.getControlador7();
                hamburgesConSalsaPiñaDecorator.armarHamburgesa();
                //aqui es donde nos damos cuenta que efectivamente se limito a solo un objeto por cada configuracion o tipo de hamburgesas
                System.out.println(hamburgesaBase.hashCode());


            }
        });

        Hilo1.start();



}}