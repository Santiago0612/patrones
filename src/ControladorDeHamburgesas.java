public class ControladorDeHamburgesas {

    //Esta clase contiene el patron Singleton Que lo que hace es limitar todas las hamburgesas a una sola referencia
    //Cumpliendo con la parte del ejercicio que dice "donde cualquier cliente pueden ejecutar registros y
    // todos miran lo mismo pues comparten la fuente de la verdad. "

    private static HamburgesaBase hamburgesaBase;
    private static HamburgesaBasicaDecorator hamburgesaBasicaDecorator;

    private static HamburgesaCompletaDecorator hamburgesaCompletaDecorator;
    private static HamburgesaConQuesoDecorator hamburgesaConQuesoDecorator;
    private static HamburgesaConSalsaBBQDecorator hamburgesaConSalsaBBQDecorator;

    private static HamburgesaConTocinetaDecorator hamburgesaConTocinetaDecorator;

    private static HamburgesaPolloDecorator hamburgesaPolloDecorator;
    private static HamburgesConSalsaPiñaDecorator hamburgesConSalsaPiñaDecorator;
    private ControladorDeHamburgesas() {
    }



    public synchronized static HamburgesaBase getControlador() {
        if (hamburgesaBase== null) {
            hamburgesaBase = new HamburgesaBase();
        }

        return hamburgesaBase;

    }
    public synchronized static HamburgesaBasicaDecorator getControlador1() {
        if (hamburgesaBasicaDecorator == null) {
            hamburgesaBasicaDecorator =new HamburgesaBasicaDecorator(hamburgesaBase);
        }

        return hamburgesaBasicaDecorator;

    }
    public synchronized static HamburgesaCompletaDecorator getControlador2() {
        if (hamburgesaCompletaDecorator == null) {
            hamburgesaCompletaDecorator =new HamburgesaCompletaDecorator(hamburgesaBasicaDecorator);
        }

        return hamburgesaCompletaDecorator;

    }
    public synchronized static HamburgesaConQuesoDecorator getControlador3() {
        if (hamburgesaConQuesoDecorator == null) {
            hamburgesaConQuesoDecorator =new HamburgesaConQuesoDecorator(hamburgesaCompletaDecorator);
        }

        return hamburgesaConQuesoDecorator;

    }
    public synchronized static HamburgesaConSalsaBBQDecorator getControlador4() {
        if (hamburgesaConSalsaBBQDecorator == null) {
            hamburgesaConSalsaBBQDecorator =new HamburgesaConSalsaBBQDecorator(hamburgesaConQuesoDecorator);
        }

        return hamburgesaConSalsaBBQDecorator;

    }
    public synchronized static HamburgesaConTocinetaDecorator getControlador5() {
        if (hamburgesaConTocinetaDecorator == null) {
            hamburgesaConTocinetaDecorator =new HamburgesaConTocinetaDecorator(hamburgesaConSalsaBBQDecorator);
        }

        return hamburgesaConTocinetaDecorator;

    }
    public synchronized static HamburgesaPolloDecorator getControlador6() {
        if (hamburgesaPolloDecorator == null) {
            hamburgesaPolloDecorator =new HamburgesaPolloDecorator(hamburgesaConTocinetaDecorator);
        }

        return hamburgesaPolloDecorator;

    }

    public synchronized static HamburgesConSalsaPiñaDecorator getControlador7() {
        if (hamburgesConSalsaPiñaDecorator == null) {
            hamburgesConSalsaPiñaDecorator =new HamburgesConSalsaPiñaDecorator(hamburgesaPolloDecorator);
        }

        return hamburgesConSalsaPiñaDecorator;

    }





}
